<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoadConttroller extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(): Response
    {
        $hue = random_int(0, 360);
        $alpha = 1;
        $darkColor = "hsla($hue, 20%, 45%, $alpha)";
        $lightColor = "hsla($hue, 20%, 95%, $alpha)";
        return $this->render('first.html.twig', [
            'darkColor' => $darkColor,
            'lightColor' => $lightColor,
            'hue' => $hue
        ]);
    }

    /**
     * @Route("/load/generator", name="load_generator")
    */
    public function generator(): Response
    {
        $number = random_int(10000, 300000);
        $mLoad = [];

        while ($number > 0) {
            $mLoad[] = random_int(0, 1500000);
            $number--;
        }

        $sum = 0;
        foreach ($mLoad as $nm) {
            $sum += $nm;
        }

        return new Response(
            '<html><body>Lucky number: '.$sum.'</body></html>'
        );
    }
}